package com.dumadugames.unityandroid;

import java.util.StringTokenizer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.dumadugames.pocketbowling.PocketBowling3D;
import com.unity3d.player.UnityPlayer;

public class JarClass {
	public static void purchaseItem(boolean managed, int id, Activity activity) {
		String itemId = "";
		if (!managed)
			switch (id) {
			case 1:
				itemId = "com.pocketbowling.removeads";
				break;
			default:
				break; 
			} 
		else switch (id) {
			case 1:
				itemId = "com.pocketbowling.coinspack100";
				break;
			case 2:
				itemId = "com.pocketbowling.coinspack250";
				break;
			case 3:
				itemId = "com.pocketbowling.coinspack750";
				break;
			case 4:
				itemId = "com.pocketbowling.coinspack1500";
			}

		PocketBowling3D.purchaseItem(itemId, activity);
		Log.e("Callback", "Purchase Item"+"    "+itemId);
	}
	
	public static void restorePurchaseItem(Activity activity){
		Log.e("Callback", "restorePurchaseItem");
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "true");
	}

	public static void showIntrestitial(Activity activity) {
		Log.e("Callback", "Full Screen Ad Displayed");
	}
	
	public static void showIntrestitial(int label, Activity activity) {
		Log.e("Callback", "lablel "+label+"Full Screen Ad Displayed");
		PocketBowling3D.showIntrestitial(activity);
	}
	
	public static void showBanner(boolean top, Activity activity) {
		PocketBowling3D.showBanner(top, activity);
	}

	public static void hideBanner(boolean top, Activity activity) {
		PocketBowling3D.hideBanner(top, activity);
	}
	
	public static void gameServicesLogin(Activity activity) {
	    Log.e("Callback", "gameServicesLogin");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
	}
	
	public static void gameServicesLogout(Activity activity) {
	    Log.e("Callback", "gameServicesLogout");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
	}

	public static void submitScore(int id, int score, Activity activity) {
		Log.e("Callback", score + " Posted to Leaderboard: int score" + id);
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
		String leaderboardId = "";
		switch (id) {
		case 1:
			leaderboardId = "CgkI8p-fh_gKEAIQAA";
		}
		PocketBowling3D.submitScore(leaderboardId, score, activity);
		Log.e("Callback", score + " Posted to Leaderboard " + id);
	}
	
	public static void shareScore(int id, int score, Activity activity) {
		Log.e("Callback", score + " score shared : int score" + id);
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my score: "+score+"  \n https://play.google.com/store/apps/details?id="+activity.getPackageName());
		activity.startActivity(Intent.createChooser(sendIntent, "Share using"));
	}
	
	public static void shareScore(int id, float score, Activity activity) {
		Log.e("Callback", score + " score shared : float score" + id);
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.setType("text/plain");
		sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my score: "+score+"  \n https://play.google.com/store/apps/details?id="+activity.getPackageName());
		activity.startActivity(Intent.createChooser(sendIntent, "Share using"));
	}
	
	public static void unlockAchievement(int id, Activity activity) {
		Log.e("Callback", id + "Achievement Unlocked: int_id");
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
		String achId = "";
		if (id == 1)
			achId = "CgkI8p-fh_gKEAIQAQ";
		else if (id == 2)
			achId = "CgkI8p-fh_gKEAIQAg";
		else if (id == 3)
			achId = "CgkI8p-fh_gKEAIQAw";
		else if (id == 4)
			achId = "CgkI8p-fh_gKEAIQBA";
		else if (id == 5)
			achId = "CgkI8p-fh_gKEAIQBQ";
		PocketBowling3D.unlockAchievement(achId, activity);
		Log.e("Callback", id + "Achievement Unlocked int");
	}
	
	public static void incrementAchievement(int id, int steps, Activity activity) {
		Log.e("Callback", "Achivement Incremented by: int_id" + steps);
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
	}
	
	public static void showLeaderBoards(Activity activity) {
		Log.e("Callback", "LeaderBoards Displayed");
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
		PocketBowling3D.showLeaderBoards(activity);
	}
	
	public static void showAchievements(Activity activity) {
		Log.e("Callback", "Achievements Displayed");
		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "GoogleSignIn_true");
		PocketBowling3D.showAchievements(activity);
	}
	
	public static void showExitPopUp(Activity activity) {
		Log.e("Callback", "Exit PopUp Displayed");
		PocketBowling3D.showExitPopUp(activity);
	}
	
	public static void submitFlurryEvent(String key, Activity activity) {
		Log.e("Callback", "FlurryEvent : " + key);
	}
	
	public static void like(Activity activity) {
		Log.e("Callback", "facebook");
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/dumadugames")));
	}
	
	public static void follow(Activity activity) {
		Log.e("Callback", "twitter");
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/dumadugames")));
	}
	
	public static void moreGames(Activity activity) {
		Log.e("Callback", "More Games");
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:Dumadu+Games")));
	}

	public static void rateApp(boolean isCallbackReq, Activity activity) {
		  Log.e("Callback", "Rate Application");
		  activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+activity.getPackageName())));
		  if (isCallbackReq) {
		   UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "true");
		  }
    }
	
	public static void showGiftiz(Activity activity) {
		Log.e("Callback", "showGiftiz");
	}
	
	public static void giftizMissionDone(Activity activity) {
		Log.e("Callback", "giftizMissionDone");
	}
	
	public static void showVideoAds(int id, Activity activity) {
	    Log.e("Callback", "showVideoAds"+"id: "+id);
	    PocketBowling3D.showVideoAds(activity);
	   // UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "VideoAds_true");//VideoAds_false
	}
	
	public static void showOfferwall(int id, Activity activity) {
	    Log.e("Callback", "showOfferwall"+"id: "+id);
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "OfferWall_true_10");//OfferWall_false_0, OfferWall_true_'any value'
	}
	
	public static void shareScreen(Activity activity) {
		Log.e("Callback", "shareScreen");
	    Intent sharingIntent = new Intent("android.intent.action.SEND");
	    sharingIntent.setType("image/png");
	    sharingIntent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + Environment.getExternalStorageDirectory().getPath() + "/ScreenShot.png"));
	    sharingIntent.putExtra("android.intent.extra.TEXT", "Hi Friends, i am playing https://play.google.com/store/apps/details?id="+activity.getPackageName());
	    activity.startActivity(Intent.createChooser(sharingIntent, "Share Image"));
	}
	
	public static void shareScreen(String path, Activity activity) {
	    Log.e("Callback", "shareScreen Path:"+path);
	}
	
	public static void localNotification(String date, String msg, int id, Activity activity) {
	    StringTokenizer tokenizer = new StringTokenizer(date, "_");
	    Log.e("Callback ", id + " notify after " + tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + " " + 
	      tokenizer.nextToken() + "   " + msg);
	}
	
	public static void cancellocalNotification(boolean notification, Activity activity) {
		Log.e("Callback", "cancellocalNotification  " + notification);
	}
	
	public static void analyticsEvent(String message, Activity activity) {
		Log.e("Callback", "Event " + message);
		PocketBowling3D.analyticsEvent(message, activity);
	}

	public static void analyticsScreen(String message, Activity activity) {
		Log.e("Callback", "Screen " + message);
		PocketBowling3D.analyticsScreen(message, activity);
	}
	
	public static void initialize(Activity activity) {
	    Log.e("Callback", "initialize");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "SetExternalPreferences", "0");
	    //0-Free, 1-Premium, 2-Custom
	    //UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "SetDefaultCurrency", "1000");
	    //UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "SetExternalPreferences", "true_true_true_true");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "LocalCurrencyValues", "$0.99_$0.99_$1.99_$1.99_$1.99_$2.99_$0.99");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "LocalCurrencyType", "USD");
	    Log.e("Callback", Environment.getExternalStorageDirectory() + "/ScreenShot.png");
	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "ScreenShotPath", Environment.getExternalStorageDirectory() + "/ScreenShot.png");
	    
//	    PocketBowling3D.initialize(activity);
	    
	    /*For Notifications*/
//	    String packageName = activity.getPackageName();
//	    Intent launchIntent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
//	    String className = launchIntent.getComponent().getClassName();
//	    Log.e("Callback", "initialize Class Name   "+className);
//	    UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "SetMainActivityClassName", "className");
	    
	    
	}
	
	public static void gamePlayStart(boolean b, Activity activity) {
		Log.e("Callback", "gamePlayStart");
	}
	
	public static void gamePlayEnd(boolean b, Activity activity) {
		Log.e("Callback", "gamePlayEnd");
	}
	
	public static void startYouTube(int levelNo, Activity activity) {
		Log.e("Callback", "started YouTube with level "+levelNo);
	}
	
	//For Alphonso
	public static void micStatus(boolean status, Activity activity) {
		Log.e("Callback", "micStatus: "+status);
//		UnityPlayer.UnitySendMessage("ExternalInterfaceHandler", "Receiver", "MicStatus_false");//MicStatus_false	
//		PocketBowling3D.micStatus(status, activity);
	}
	
	//Multi Player Calls
	public static void startMultiPlayer(int isWifi, Activity activity) {
	    Log.e("Callback", "isWifi: "+isWifi);
	    PocketBowling3D.startMultiPlayer(isWifi, activity);
	}
	public static void sendCurrentPlayerName(int isWifi, String name, Activity activity) {
		Log.e("Callback", "isWifi: "+isWifi +"name: "+name);
		PocketBowling3D.sendCurrentPlayerName(isWifi, name, activity);
	}
	public static void sendShotScore(int isWifi, int score, Activity activity) {
		Log.e("Callback", "isWifi: "+isWifi +"score: "+score);
		PocketBowling3D.sendShotScore(isWifi, score, activity);
	}
	public static void quitGameSession(int isWifi, String heading, String msg, Activity activity) {
		Log.e("Callback", "isWifi: "+isWifi +"heading: "+heading +"message: "+msg);
		PocketBowling3D.quitGameSession(isWifi, heading, msg, activity);
	}
	
	//for Stupid Birds
	public static boolean inappStatus(Activity activity) {
	    return true;
	}
	//for Perk points(Appsholic)
	public static void showPerkAchievements(Activity activity) {
		Log.e("Callback", "Perk Achievements Displayed");
	}
	public static void unlockPerkAchievement(int id, Activity activity) {
		Log.e("Callback", id + "Perk Achievement Unlocked int");
	}
	
	//Just Loot 
	public static void justlootAppSignInRequest(String pListener, Activity activity) {
		UnityPlayer.UnitySendMessage(pListener, "Receiver", "true");
		Log.e("Callback", "lootAppSignInRequest: "+"pListener: "+pListener);
	}
	public static void claimLootPoints(String pListener, int pUnclaimedPoints, Activity activity) {
		UnityPlayer.UnitySendMessage(pListener, "Receiver", "true");
        Log.e("Callback", "claimLootPoints: "+"pListener: "+pListener+"pUnclaimedPoints: "+pUnclaimedPoints);
	}
	public static void requestTotalLootPoints(String pListener, Activity activity) {
		UnityPlayer.UnitySendMessage(pListener, "Receiver", "true");
		Log.e("Callback", "requestTotalLootPoints: "+"pListener: "+pListener);
	}
	public static void lootAppDeepLink(int pType, Activity activity) {
		Log.e("Callback", "lootAppDeepLink: "+"pType: "+pType);
	}
	public static void justlootAppSignStatus(String pListener, Activity activity) {
		UnityPlayer.UnitySendMessage(pListener, "Receiver", "");//""-if no user or mobile number/first name
		Log.e("Callback", "justlootAppSignStatus: "+"pListener: "+pListener);
	}
	
	
}