package com.dumadugames.pocketbowling;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.dumadu.google.playgames.BaseGameActivity;
import com.dumadu.iab.util.IabHelper;
import com.dumadu.iab.util.IabHelper.OnConsumeFinishedListener;
import com.dumadu.iab.util.IabHelper.OnIabPurchaseFinishedListener;
import com.dumadu.iab.util.IabHelper.OnIabSetupFinishedListener;
import com.dumadu.iab.util.IabHelper.QueryInventoryFinishedListener;
import com.dumadu.iab.util.IabResult;
import com.dumadu.iab.util.Inventory;
import com.dumadu.iab.util.Purchase;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMultiplayer.ReliableMessageSentCallback;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.unity3d.player.UnityPlayer;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PocketBowling3D extends BaseGameActivity implements OnInvitationReceivedListener, RoomUpdateListener, RealTimeMessageReceivedListener, RoomStatusUpdateListener, RewardedVideoAdListener
{
	private UnityPlayer mUnityPlayer;
	private FrameLayout layout;
	
	private static GoogleApiClient gamesClient;
	
	private static Handler handler;
	private static final int GOOGLE_LOGIN = 1;
	private static final int SHOW_BANNER_BOTTOM = 2;
	private static final int SHOW_BANNER_TOP = 3;
	private static final int HIDE_BANNER = 4;
	private static final int SHOW_INTRESTITIAL = 5;
	private static final int SHOW_POPUP = 6;
	private static final int SHOW_EXIT = 7;
	private static final int QUICK_GAME = 8;
	private static final int TOAST = 9;
	private static final int START_ALPHONSO = 10;
	private static final int STOP_ALPHONSO = 11;
	private static final int SHOW_VIDEO = 12;

	private final static int RC_SELECT_PLAYERS = 10000;
	private final static int RC_INVITATION_INBOX = 10001;
    private final static int RC_WAITING_ROOM = 10002;
	private boolean waitRoomDismissedFromCode = false;
	private static String roomId = null;
    private static ArrayList<Participant> participants = null;
    
    private static String myId = null;
    private static byte[] msgBuf = new byte[4];
	private static String toastMsg;
	
	private InterstitialAd interstitialAd;
	private AdView banner;

	private RewardedVideoAd mRewardedVideoAd;
    private boolean mIsRewardedVideoLoading;
    private final Object mLock = new Object();
    boolean videoStatus = false;
	private static boolean exit;
	
	private static boolean isFull;
	private static boolean isBillingSupported;
	private static final int    RC_INAPP = 20001;
	private static final String REMOVE_ADS_SKU = "com.pocketbowling.removeads";
	private static final String COINS_SKU_100 = "com.pocketbowling.coinspack100";
	private static final String COINS_SKU_250 = "com.pocketbowling.coinspack250";
	private static final String COINS_SKU_750 = "com.pocketbowling.coinspack750";
	private static final String COINS_SKU_1500 = "com.pocketbowling.coinspack1500";
	private static IabHelper billingHelper;
	private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl+6G9PDp8jOGwfRaAIZoWPipWIxMBQ13H1dMHWY2ZHVbxozhtl+UcHbm+39o9xWKarVj1VrcT2m37u1NH1ii56XTtC0VkJKC9ttr6mtbNlWABKLUy3I6d/Bws4ugUPJSoDB95qmUzYNGsfyuDc0tngaa7//9MWwmSrBgqDa5u4/SkjIjiHrK/CevT42c/cECc0CH2rkk9UDfimVJNHEnx80okG5JxZVfFoDKlr7cjJoKph47bdPtMoop/jELGwnKqJt/hvubmDaAbUZbCZ3Gi+by3jh97MbWh4g3DoH8N9Y4Z0vgPVJdqzzzUFjoeVfztYj8dPhadBFCsoyPuhdX1QIDAQAB";
//	private static final String SLIDEME_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA9CIZK2Gpy1hwiawcRmbQ/Bb6rfoCvy3neCZoVcngRiD0nvih5khbG9HoWsN8mHkITJkyp71PCO9D4Sxdx3j2mBxtbaKwC0MCh4C87Q2MyFI7TuzBJmZoMwAQT/JNJBOa4xuoDDSRwFv1I5nfCqWili8tlOUZSCBgvt7Q74AU/sJiF664uWJlvy2gQskYY2lbg/n9hKID6wKX47pABjUTO36j2hYKpc40Q6ACljp+tp9ZUoE7MMgok1Kw6VNXCeKwOffpKe7xNt6pnOjILRunQKB81qLw997V2WBS60k97k9GdWfPG+ZlqAdc2b6ZHcBPDQbAwRPuADMHizZFB59YgQIDAQAB";
	
	private View mDecorView;
	
	private ProgressDialog dailog;

	private static Toast toast;
	boolean isVideoLoading;
	
	//Alphonso 
	private static final String TAG     = "Callback";
	private static final String APP_ID  = "D-P-B-3-A-BWoRl8C9oONGbRQs";//"Alphonso-android-sdktest-app-API-key-5g7sd290B";//
	private ResultReceiver    mStatusReceiver          = null;
	private ResultReceiver    mIdentificationReceiver  = null;
	private static Activity activity;
	final Handler handler_alphonso = new Handler();
	static String countryName;
	private static boolean micStatus, firstLaunch;//false-its a first launch, true- its a not a first launch
	SharedPreferences sharedpreferences;
	static Editor editor;

	//Firebase Analytics
	private static FirebaseAnalytics mFirebaseAnalytics;

	// Setup activity layout
	@SuppressLint("HandlerLeak")
	@Override protected void onCreate (Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);

		setContentView(R.layout.unity_game_screen);
		layout = (FrameLayout) findViewById(R.id.game_view);
		
		getWindow().takeSurface(null);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		getWindow().setFormat(PixelFormat.RGB_565);
		
		mUnityPlayer = new UnityPlayer(this);
		layout.addView(mUnityPlayer.getView());

		//Firebase Analytics
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
		
		mDecorView = getWindow().getDecorView();
		dailog = new ProgressDialog(this);
		dailog.setMessage("Initializing...");
		
		//Admob Rewarded videos
		mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();
		
		//Alphonso
		activity = this;
		countryName = this.getResources().getConfiguration().locale.getCountry();
		Log.e("test", "COUNTRY: "+countryName);
//		if (checkPermission_RecordAudio()) {
//			setupAlphonsoService();	
//		}else {
//			requestPermission();
//		}
		
		//Alphonso Mic Status
		sharedpreferences = getSharedPreferences("AlphonsoMicStatus", Context.MODE_PRIVATE);
		editor = sharedpreferences.edit();
		micStatus = sharedpreferences.getBoolean("micStatus", false);
		firstLaunch = sharedpreferences.getBoolean("firstLaunch", false);
		
		banner = ( AdView) findViewById(R.id.banner);
		interstitialAd = new  InterstitialAd(this);
		interstitialAd.setAdUnitId(getString(R.string.dfp_interstitial));
		interstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				// TODO Auto-generated method stub
				if (exit) {
				//	finish();
					nativeExitPopup();
					return;
				}else {
					if (isNetworkAvailable()) {
						if (!isFull) {
							loadInterstitialAd();
						}
					}
				}
			}

			@Override
			public void onAdOpened() {
				super.onAdOpened();
				//total ads shown event
				adsanalyticsEvent("IntShown", null);
			}
		});
		
//		Map<String, String> storeKeys = new HashMap<String, String>();
//		storeKeys.put(IabHelper.NAME_GOOGLE, base64EncodedPublicKey);
//		storeKeys.put("SlideME", SLIDEME_PUBLIC_KEY);
		
		billingHelper = new IabHelper(this, base64EncodedPublicKey);
		billingHelper.startSetup(new OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				// TODO Auto-generated method stub
				if (!result.isSuccess()) {
					isBillingSupported = false;
					return;
				}
				isBillingSupported = true;
				billingHelper.queryInventoryAsync(inventoryFinishedListener);
			}
		});
		
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				switch (msg.what) {
				case GOOGLE_LOGIN:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							beginUserInitiatedSignIn();
						}
					});
					break;
				case SHOW_BANNER_TOP:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
							params.addRule(RelativeLayout.CENTER_HORIZONTAL);
							params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
							banner.setLayoutParams(params);
							banner.setVisibility(View.VISIBLE);
						}
					});
					break;
				case SHOW_BANNER_BOTTOM:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
							params.addRule(RelativeLayout.CENTER_HORIZONTAL);
							params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
							banner.setLayoutParams(params);
							banner.setVisibility(View.VISIBLE);
						}
					});
					break;
				case HIDE_BANNER:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							banner.setVisibility(View.GONE);
						}
					});
					break;
				case SHOW_INTRESTITIAL:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							if (!isFull) {
								adsanalyticsEvent("IntGameReq", null);
								if (interstitialAd.isLoaded()) {
									interstitialAd.show();
								}else {
									loadInterstitialAd();
								}
							}
						}
					});
					break;
				case SHOW_EXIT:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							adsanalyticsEvent("EXIT_ADS", null);
							if (!exit && interstitialAd.isLoaded() && !isFull) {
								interstitialAd.show();
								exit = true;
							} else {
								//	finish();
								nativeExitPopup();

							}
						}
					});
					break;
				case QUICK_GAME:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							startQuickGame();
						}
					});
					break;
				case TOAST:
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
						}
					});
					break;
				case SHOW_VIDEO:
					runOnUiThread(new Runnable() {
						public void run() {
							if (mRewardedVideoAd.isLoaded()) {
                                mRewardedVideoAd.show();
                                Log.e(TAG, "isLoaded");
                            }else {
                            	toastMsg = "No Videos Available..";
                                try{
                                    toast.getView().isShown();     // true if visible
                                    toast.setText(toastMsg);
                                } catch (Exception e) {         // invisible if exception
                                    toast = Toast.makeText(PocketBowling3D.this, toastMsg, Toast.LENGTH_SHORT);
                                }
                                toast.show();
                                UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","false");
                                Log.e(TAG, "Video Ad Failed: is not Loaded");
                                Log.e(TAG, "is not Loaded");
                                loadRewardedVideoAd();
                            }
						}
					});
					break;
				}
			}
		};
		
		if (android.os.Build.VERSION.SDK_INT >= 19) {
			UiChangeListener();
		}
		//Update Alert
//		new UpdateAlert(PocketBowling3D.this);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (!isFull) {
			loadBanner();
			loadInterstitialAd();
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mUnityPlayer.pause();
		if (isFinishing())
			mUnityPlayer.quit();
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mUnityPlayer.resume();
	}
	
	@Override
	protected void onDestroy ()	{
		super.onDestroy();
		mUnityPlayer.quit();
		//Alphonso
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig)	{
		super.onConfigurationChanged(newConfig);
		mUnityPlayer.configurationChanged(newConfig);
	}
	
	// Notify Unity of the focus change.
	@Override public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		mUnityPlayer.windowFocusChanged(hasFocus);
		if (hasFocus && android.os.Build.VERSION.SDK_INT >= 19) {
			mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		}
	}

	// For some reason the multiple keyevent type is not supported by the ndk.
	// Force event injection by overriding dispatchKeyEvent().
	@Override public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return mUnityPlayer.injectEvent(event);
		return super.dispatchKeyEvent(event);
	}

	// Pass any events not handled by (unfocused) views straight to UnityPlayer
	@Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
	/*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return mUnityPlayer.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)	{
		return mUnityPlayer.onKeyUp(keyCode, event);
	}
	
	//Callback Methods
	public static void purchaseItem(String itemId, Activity activity) {
		if (isFull && itemId.equals(REMOVE_ADS_SKU)) {
			UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
			toastMsg = "You are already owned this item";
			Message message = new Message();
			message.what = TOAST;
			handler.handleMessage(message);
		} else {
			if (isBillingSupported) {
				billingHelper.launchPurchaseFlow(activity, itemId, RC_INAPP, purchaseFinishedListener, null);
			} else {
				toastMsg = "Sorry, this service is not available";
				Message message = new Message();
				message.what = TOAST;
				handler.handleMessage(message);
			}
		}
	}
	
	public static void submitScore(String leaderboardId, int score, Activity activity) {
		if (gamesClient != null) {
			Games.Leaderboards.submitScore(gamesClient, leaderboardId, score);
		} else {
			SharedPreferences preferences = activity.getSharedPreferences("GameServices", Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putInt(leaderboardId, Math.max(score, preferences.getInt(leaderboardId, 0)));
			editor.commit();
		}
	}
	
	public static void unlockAchievement(String id, Activity activity) {
		if (gamesClient != null) {
			Games.Achievements.unlock(gamesClient, id);
		} else {
			SharedPreferences preferences = activity.getSharedPreferences("GameServices", Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putBoolean(id, true);
			editor.commit();
		}
	}
	
	public static void incrementAchievement(String id, int steps, Activity activity) {
		if (gamesClient != null && steps >= 2) {
			Games.Achievements.increment(gamesClient, id, steps);
		}
	}

	public static void adsanalyticsEvent(String message, Activity activity) {
		Log.e("test", "Firebase Analytics: " + message);
//		StringTokenizer tokenizer = new StringTokenizer(message, "_");
//		tracker.send(new HitBuilders.EventBuilder().setCategory(tokenizer.nextToken()).setAction(tokenizer.nextToken()).setLabel(tokenizer.nextToken()).build());
//		String eventName = tokenizer.nextToken();//Category
//		String action = tokenizer.nextToken();//Action
//		String label = tokenizer.nextToken();//Label

//		Bundle bundle = new Bundle();
//		bundle.putString("Action", "");
//		bundle.putString("Label", "");
		mFirebaseAnalytics.logEvent(message, null);
	}

	public static void gameanalyticsEvent(String message, Activity activity) {
		Log.e("test", "Firebase Analytics: " + message);
		StringTokenizer tokenizer = new StringTokenizer(message, "_");
//		tracker.send(new HitBuilders.EventBuilder().setCategory(tokenizer.nextToken()).setAction(tokenizer.nextToken()).setLabel(tokenizer.nextToken()).build());
		String category = tokenizer.nextToken();//Category
		String action = tokenizer.nextToken();//Action
		String label = tokenizer.nextToken();//Label
		String value = "";

		Bundle bundle = new Bundle();
//		bundle.putString("Action", action);
		bundle.putString("Label", label);
		mFirebaseAnalytics.logEvent(action, bundle);
	}

	public static void popUp(String msg, Activity activity) {
		Message message = new Message();
		message.what = SHOW_POPUP;
		handler.handleMessage(message);
	}
	
	public static void showLeaderBoards(final Activity activity) {
		if (gamesClient != null) {
			activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gamesClient, activity.getString(R.string.leaderboard_high_score)), 5001);
		} else {
			Message message = new Message();
			message.what = GOOGLE_LOGIN;
			handler.handleMessage(message);
		}
	}
	
	public static void showAchievements(Activity activity) {
		if (gamesClient != null) {
			activity.startActivityForResult(Games.Achievements.getAchievementsIntent(gamesClient), 5001);
		} else {
			Message message = new Message();
			message.what = GOOGLE_LOGIN;
			handler.handleMessage(message);
		}
	}
	
	public static void showIntrestitial(Activity activity)  {
		if (!isFull) {
			Message message = new Message();
			message.what = SHOW_INTRESTITIAL;
			handler.handleMessage(message);
		}
	}
	
	public static void showBanner(final boolean top, Activity activity)  {
		if (!isFull) {
			if (top) {
				Message message = new Message();
				message.what = SHOW_BANNER_TOP;
				handler.handleMessage(message);
			} else {
				Message message = new Message();
				message.what = SHOW_BANNER_BOTTOM;
				handler.handleMessage(message);
			}
		}
	}
	
	public static void hideBanner(final boolean top, Activity activity)  {
		Message message = new Message();
		message.what = HIDE_BANNER;
		handler.handleMessage(message);
	}
	
	public static void showVideoAds(Activity activity)  {
		analyticsEvent("Rewarded_Total RewardedVideo requests from game_Total RewardedVideo requests from game", null);
		Message message = new Message();
		message.what = SHOW_VIDEO;
		handler.handleMessage(message);
//		Log.e("call", "i: "+i);
//		if (i == 1) {
//			i++;
//			if (UnityAds.canShow() && UnityAds.canShowAds()) {
//				UnityAds.show();
//			} else if (VunglePub.isVideoAvailable()) {
//				VunglePub.displayIncentivizedAdvert(true);
//			} else {
//				showToastMsg("No Videos Available", activity);
//				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","false");
//			}
//		}else if (i == 2) {
//			i--;
//			if (VunglePub.isVideoAvailable()) {
//				VunglePub.displayIncentivizedAdvert(true);
//			} else if (UnityAds.canShow() && UnityAds.canShowAds()) {
//				UnityAds.show();
//			} else {
//				showToastMsg("No Videos Available", activity);
//				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","false");
//			}
//		}
		
	}
	
	public static void showExitPopUp(Activity activity) {
		Message message = new Message();
		message.what = SHOW_EXIT;
		handler.handleMessage(message);
	}
	
	public static void like(Activity activity) {
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/dumadugames")));
	}
	
	public static void follow(Activity activity) {
		activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/dumadugames")));
	}
	
	public void onSignInFailed() {
		// TODO Auto-generated method stub
		gamesClient = null;
		Log.e("test", "14 Receiver");
	}
	
	public void onSignInSucceeded() {
		// TODO Auto-generated method stub
		gamesClient = getApiClient();
		SharedPreferences preferences = getSharedPreferences("GameServices", Context.MODE_PRIVATE);
		boolean sync = preferences.getBoolean("sync", false);
		if (!sync) {
			updateGameServices(preferences);
			Editor editor = preferences.edit();
			editor.putBoolean("sync", true);
			editor.commit();
		}
	}

	private void updateGameServices(SharedPreferences preferences) {
		// TODO Auto-generated method stub
		Games.Leaderboards.submitScore(gamesClient, getString(R.string.leaderboard_high_score), preferences.getInt(getString(R.string.leaderboard_high_score), 0));
		if (preferences.getBoolean(getString(R.string.achievement_amateur), false))
			Games.Achievements.unlock(gamesClient, getString(R.string.achievement_amateur));
		if (preferences.getBoolean(getString(R.string.achievement_novice), false))
			Games.Achievements.unlock(gamesClient, getString(R.string.achievement_novice));
		if (preferences.getBoolean(getString(R.string.achievement_intermediate), false))
			Games.Achievements.unlock(gamesClient, getString(R.string.achievement_intermediate));
		if (preferences.getBoolean(getString(R.string.achievement_advanced), false))
			Games.Achievements.unlock(gamesClient, getString(R.string.achievement_advanced));
		if (preferences.getBoolean(getString(R.string.achievement_expert), false))
			Games.Achievements.unlock(gamesClient, getString(R.string.achievement_expert));
		
	}
	//Google Play Services Multiplayer code ------------------------------------------- START
	//Multiplayer
	public static void startMultiPlayer(int isWifi, Activity activity) {
		if (isWifi == 1) {
			if (gamesClient != null) {
				Message message = new Message();
				message.what = QUICK_GAME;
				handler.handleMessage(message);
			} else {
				UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","false");
				Message message = new Message();
				message.what = GOOGLE_LOGIN;
				handler.handleMessage(message);
			}
		}
	}
	
	public static void sendCurrentPlayerName(int isWifi, String name, Activity activity) {
		UnityPlayer.UnitySendMessage("MultiplayerCommunicator","NameReceiver","Player2");
	}
	public static void sendShotScore(int isWifi, int score, Activity activity) {
		if (isWifi == 1) {
			sendMessage(""+score, activity);
		}
	}	
	public static void quitGameSession(int isWifi, String heading, String msg, Activity activity) {
		if (isWifi == 1&& roomId != null) {
			Games.RealTimeMultiplayer.leave(gamesClient, new RoomUpdateListener() {
				
				@Override
				public void onRoomCreated(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onRoomConnected(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLeftRoom(int statusCode, String roomId) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onJoinedRoom(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
			}, roomId);
			roomId = null;
	    }
		UnityPlayer.UnitySendMessage("MultiplayerCommunicator", "ConnectionLost", "true");
	}
   
	@Override
	public void onConnectedToRoom(Room arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDisconnectedFromRoom(Room room) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onP2PConnected(String participantId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onP2PDisconnected(String participantId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPeerDeclined(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPeerInvitedToRoom(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPeerJoined(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPeerLeft(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
		if (roomId != null) {
			Games.RealTimeMultiplayer.leave(gamesClient, new RoomUpdateListener() {
				
				@Override
				public void onRoomCreated(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onRoomConnected(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onLeftRoom(int statusCode, String roomId) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onJoinedRoom(int statusCode, Room room) {
					// TODO Auto-generated method stub
					
				}
			}, roomId);
			roomId = null;
	    }
		toastMsg = "Other Player Left from Room";
		Message message = new Message();
		message.what = TOAST;
		handler.handleMessage(message);
		UnityPlayer.UnitySendMessage("MultiplayerCommunicator", "ConnectionLost", "true");
	}

	@Override
	public void onPeersConnected(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPeersDisconnected(Room arg0, List<String> arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRoomAutoMatching(Room room) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRoomConnecting(Room room) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRealTimeMessageReceived(RealTimeMessage message) {
		// TODO Auto-generated method stub
		try {
			String str = new String(message.getMessageData(), "UTF-8");
			UnityPlayer.UnitySendMessage("MultiplayerCommunicator","DataReceiver",str);
		} catch (UnsupportedEncodingException e) {
		}
	}

	@Override
	public void onJoinedRoom(int statusCode, Room room) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLeftRoom(int statusCode, String roomId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRoomConnected(int statusCode, Room room) {
		// TODO Auto-generated method stub
		if (statusCode != GamesStatusCodes.STATUS_OK) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","false");
            return;
        } else {
        	roomId = room.getRoomId();
            participants = room.getParticipants();
            Log.e("test", "PARTICIPENTS: "+participants.get(0));
            myId = room.getParticipantId(Games.Players.getCurrentPlayerId(getApiClient()));
			dismissWaitingRoom();
//			String[] str = new String[2];
//			str[0] = room.getParticipants().get(0).getParticipantId();
//			str[1] = room.getParticipants().get(1).getParticipantId();
//			Arrays.sort(str);
//			boolean myTurn = str[0].equals(myId);
			UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","true");
//			if (myTurn) {
//			} else {
//			}
		}
	}

	@Override
	public void onRoomCreated(int statusCode, Room room) {
		// TODO Auto-generated method stub
		if (dailog.isShowing()) {
			dailog.dismiss();
		}
		if (statusCode != GamesStatusCodes.STATUS_OK) {
            Toast.makeText(this, "Something went wrong, check your internet connection", Toast.LENGTH_LONG).show();
            UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","false");
            return;
        }
        showWaitingRoom(room);
	}

	@Override
	public void onInvitationReceived(Invitation invitation) {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected void onActivityResult(int request, int response, Intent data) {
		// TODO Auto-generated method stub
		Log.e("test", "req: "+request+" resp: "+response+" data: "+data);
		if (!billingHelper.handleActivityResult(request, response, data)) {
			super.onActivityResult(request, response, data);
			switch (request) {
				case RC_SELECT_PLAYERS:
					break;
				case RC_INVITATION_INBOX:
					break;
				case RC_WAITING_ROOM:
					if (waitRoomDismissedFromCode) break;
					if (response == Activity.RESULT_OK) {
						UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","true");
					} else if (response == GamesActivityResultCodes.RESULT_LEFT_ROOM) {
						UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","false");
					} else if (response == Activity.RESULT_CANCELED) {
						UnityPlayer.UnitySendMessage("MultiplayerCommunicator","Receiver","false");
					} 
					break;
			}
        }
	}
	
	private void startQuickGame() {
        final int MIN_OPPONENTS = 1, MAX_OPPONENTS = 1;
        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(MIN_OPPONENTS, MAX_OPPONENTS, 0);
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
        rtmConfigBuilder.setVariant(2);//to filter the players with mode variants 
        Games.RealTimeMultiplayer.create(gamesClient, rtmConfigBuilder.build());
        dailog.show();
    }
	
	private void showWaitingRoom(Room room) {
        waitRoomDismissedFromCode = false;
        final int MIN_PLAYERS = 1;
        Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(gamesClient, room, MIN_PLAYERS);
        startActivityForResult(i, RC_WAITING_ROOM);
    }
	
	private void dismissWaitingRoom() {
        waitRoomDismissedFromCode = true;
        finishActivity(RC_WAITING_ROOM);
    }
	
	public static void sendMessage(String msg, Activity activity) {
		msgBuf = msg.getBytes();
		for (Participant p : participants) {
            if (p.getParticipantId().equals(myId))
                continue;
            if (p.getStatus() != Participant.STATUS_JOINED)
                continue;
            Games.RealTimeMultiplayer.sendReliableMessage(gamesClient, new ReliableMessageSentCallback() {
				@Override
				public void onRealTimeMessageSent(int statusCode, int tokenId,
						String recipientParticipantId) {
					// TODO Auto-generated method stub
					switch (statusCode) {
					case GamesStatusCodes.STATUS_OK:
						break;
					case GamesStatusCodes.STATUS_REAL_TIME_MESSAGE_SEND_FAILED:
						break;
					case GamesStatusCodes.STATUS_REAL_TIME_ROOM_NOT_JOINED:
						break;
					}					
				}
			}, msgBuf, roomId, p.getParticipantId());
        }
	}
	//Google Play services multiplayer code ----------------------------------------------------------------   END

	private static QueryInventoryFinishedListener inventoryFinishedListener = new QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			// TODO Auto-generated method stub
			 if (result.isFailure()) {
				 return;
	         }
			 Purchase purchase = inv.getPurchase(REMOVE_ADS_SKU);
			 if (purchase != null) {
				isFull = true;
			 }
		}
	};
	
	private static OnIabPurchaseFinishedListener purchaseFinishedListener = new OnIabPurchaseFinishedListener() {
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase info) {
			// TODO Auto-generated method stub
			if (result.isFailure()) {
                UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","false");
                return;
            }
			if (info.getSku().equals(REMOVE_ADS_SKU)) {
				isFull = true;
				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
			} else if (info.getSku().equals(COINS_SKU_100)
					|| info.getSku().equals(COINS_SKU_250)
					|| info.getSku().equals(COINS_SKU_750)
					|| info.getSku().equals(COINS_SKU_1500)) {
				billingHelper.consumeAsync(info, consumeFinishedListener);
			}
		}
	};
	
	private static OnConsumeFinishedListener consumeFinishedListener = new OnConsumeFinishedListener() {
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			// TODO Auto-generated method stub
			if (result.isSuccess()) {
				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
			} else {
				UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","false");
			}
		}
	};

	@Override
	public void onInvitationRemoved(String arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void UiChangeListener() {
        final View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
            }
        });
    }
	
	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager  = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}
	
	public static void analyticsEvent(String message, Activity activity) {
		StringTokenizer tokenizer = new StringTokenizer(message, "_");

	}
	
	public static void analyticsScreen(String message, Activity activity) {
	}
	public void nativeExitPopup(){
		AlertDialog.Builder alertDialogBuilder;
//		if (android.os.Build.VERSION.SDK_INT >= 11) {
//			alertDialogBuilder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
//		}else {
			alertDialogBuilder = new AlertDialog.Builder(this);
//		}
		alertDialogBuilder.setTitle(getString(R.string.app_name));
		alertDialogBuilder
		.setMessage("Do you want to exit.")
		.setCancelable(false)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				PocketBowling3D.this.finish();
			}
		  })
		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
				exit = false;
				if (!isFull) {
					loadInterstitialAd();
				}
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	/*public void showCustomAlert(String reason){
		Context context = getApplicationContext();
		LayoutInflater inflater = getLayoutInflater();
		 
		View toastRoot = inflater.inflate(R.layout.layout_toast, null);
		TextView text = (TextView) toastRoot.findViewById(R.id.text);
		text.setText(reason);
		Toast toast = new Toast(context);
		toast.setView(toastRoot);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP,0, 0);
		toast.setDuration(Toast.LENGTH_SHORT);
		toast.show();
	}*/
	

	private void loadRewardedVideoAd() {
		if (!mRewardedVideoAd.isLoaded() && !isVideoLoading && isNetworkAvailable()) {
			isVideoLoading = true;
			Bundle extras = new Bundle();
			extras.putBoolean("_noRefresh", true);
			AdRequest adRequest = new AdRequest.Builder()
					.addNetworkExtrasBundle(AdMobAdapter.class, extras)
					.build();
			mRewardedVideoAd.loadAd(getString(R.string.admob_rewarded_video), adRequest);
		}
	}

	private void loadInterstitialAd(){
		if (!interstitialAd.isLoading() || !interstitialAd.isLoaded()) {
			if (isNetworkAvailable()) {
				interstitialAd.loadAd(new AdRequest.Builder().build());
				Log.e("Ads", "Interstitial Ad Loading");
			}
		}
	}

	private void loadBanner(){
		if (!banner.isLoading()){
			if (isNetworkAvailable()){
				banner.loadAd(new AdRequest.Builder().build());
			}
		}
	}
	    
	    
		@Override
		public void onRewarded(RewardItem arg0) {
			// TODO Auto-generated method stub
			adsanalyticsEvent("RewardedVideoRewarded", null);

			UnityPlayer.UnitySendMessage("ExternalInterfaceHandler","Receiver","true");
	        videoStatus = true;
	        Log.e(TAG, "onRewarded: Video Stats: "+videoStatus);
		}

		@Override
		public void onRewardedVideoAdClosed() {
			// TODO Auto-generated method stub
			loadRewardedVideoAd();
			Log.e("Test", "onRewardedVideoAdClosed");

			
		}

		@Override
		public void onRewardedVideoAdFailedToLoad(int arg0) {
			// TODO Auto-generated method stub
			Log.e(TAG, "onRewardedVideoAdFailedToLoad");
			Log.e("Test", "onRewardedVideoAdFailedToLoad");

			isVideoLoading = false;
			loadRewardedVideoAd();
		}

	@Override
	public void onRewardedVideoCompleted() {

	}

	@Override
		public void onRewardedVideoAdLeftApplication() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRewardedVideoAdLoaded() {
			// TODO Auto-generated method stub
			isVideoLoading = false;
			Log.e("Test", "onRewardedVideoAdLoaded");

			}

		@Override
		public void onRewardedVideoAdOpened() {
			// TODO Auto-generated method stub
			Log.e("Test", "onRewardedVideoAdOpened");

			
		}

		@Override
		public void onRewardedVideoStarted() {
			// TODO Auto-generated method stub
			adsanalyticsEvent("Rewarded_RewardedVideo started_RewardedVideo started", null);
		}
	   
	
}
