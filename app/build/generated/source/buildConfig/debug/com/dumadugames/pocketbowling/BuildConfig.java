/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.dumadugames.pocketbowling;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.dumadugames.pocketbowling";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 43;
  public static final String VERSION_NAME = "2.4.2";
}
